# Copyright (C) 2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/apps/GoogleCamera/system,system)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/apps/GoogleCamera/vendor,$(TARGET_COPY_OUT_VENDOR))

PRODUCT_PACKAGES += \
    ARCore \
    Playground \
    GoogleCamera

PRODUCT_PROPERTY_OVERRIDES += \
    camera.disable_zsl_mode=true \
    sys.display-size=3840x2160
